import java.util.*;

class CustomIntComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        System.out.println("Test o1=" + o1 + " | o2=" + o2 + " result = " +(o1 < o2 ? 1 : -1));
        return o1 > o2 ? 1 : -1;
    }
}