public class Dog {
    String name;
    int age, trickScore;

    public Dog(String n, int a, int t)
    {
        name=n;
        age=a;
        trickScore=t;
    }

    public String toString(){
        return String.format(name + ", age "+age+", trickScore = "+trickScore);
    }
}
