import Bank.models.AccountType;

import java.util.*;

public class Test {
    public static void main(String args[])
    {
        //TestingPQueue1();
        //TestingPQueue2();

        String a;
        String b = "";
        String c = "        ";
        String d = "asda";

        //System.out.println(b.trim().isEmpty());
        //System.out.println(c.trim().isEmpty());
        //System.out.println(d.isEmpty());

        Enum<AccountType> z = AccountType.valueOf("SAVINGS");
        System.out.println(z);
    }

    public static void TestingPQueue2()
    {
        Dog a = new Dog("Lucky", 5, 10);
        Dog b = new Dog("Boi", 20, 999);
        Dog c = new Dog("Lazy", 10, 0);
        Dog d = new Dog("Kitty", 1, 100);

        //PriorityQueue<Dog> pQueueDogs = new PriorityQueue<Dog>(new CustomDogComparatorAge());
        PriorityQueue<Dog> pQueueDogs = new PriorityQueue<Dog>(new CustomDogComparatorAge());
        pQueueDogs.add(a);
        pQueueDogs.add(b);
        pQueueDogs.add(c);
        pQueueDogs.add(d);

        while (!pQueueDogs.isEmpty()) {
            System.out.println("\n"+pQueueDogs);
            System.out.println(pQueueDogs.poll());
        }
    }

    public static void TestingPQueue1()
    {
        // Creating empty priority queue
        PriorityQueue<Integer> pQueue = new PriorityQueue<Integer>(new CustomIntComparator2());
        // Adding items to the pQueue using add()
        pQueue.add(5);
        pQueue.add(10);
        pQueue.add(20);
        pQueue.add(15);
        pQueue.add(30);
        pQueue.add(13);
        pQueue.add(-30);
        pQueue.add(100);
        pQueue.add(1);
        pQueue.add(7);
        // Printing the top element of PriorityQueue
        //System.out.println(pQueue.peek());
        // Printing the top element and removing it
        // from the PriorityQueue container
        //System.out.println(pQueue.poll());
        // Printing the top element again


        while (!pQueue.isEmpty()) {
            System.out.println("\n"+pQueue);
            System.out.println(pQueue.poll());
        }
    }
}

class CustomIntComparator2 implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        System.out.println("Test o1=" + o1 + " | o2=" + o2 + " result = " +(o1 < o2 ? 1 : -1));
        return o1 < o2 ? 1 : -1;
    }
}



class CustomDogComparatorAge implements Comparator<Dog> {
    public int compare(Dog d1, Dog d2) {
        return d1.age < d2.age ? 1 : -1;
    }
}

class CustomDogComparatorTrickScore implements Comparator<Dog> {
    public int compare(Dog d1, Dog d2) {

        return d1.trickScore > d2.trickScore ? 1 : -1;
    }
}