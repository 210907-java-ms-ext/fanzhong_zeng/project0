package Bank.app;

import Bank.models.Account;
import Bank.models.AccountType;
import Bank.models.Transaction;
import Bank.models.User;
import Bank.services.AccountService;
import Bank.services.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class BankMain {

    UserService userService = new UserService();
    AccountService accountService = new AccountService();

    public static void main(String[] args) {
        BankMain app = new BankMain();
        app.menu();
    }

    //First screen you see when the app is launched. Givens 3 options of login, create new account, or exit program
    public void menu(){

        while (true)
        {
            System.out.println("\nWelcome to this Java Console Bank");
            System.out.println("Created by: FanZhong Zeng");
            System.out.println("Choose an action:");
            System.out.println("1 - login");
            System.out.println("2 - create new account");
            System.out.println("3 - exit program");

            Scanner scanner = new Scanner(System.in);
            try {
                int selection = scanner.nextInt();

                switch(selection){
                    case 1:
                        logIn();
                        break; // important for preventing case 2 from also being executed
                    case 2:
                        newUser();
                        break;
                    case 3:
                        return;
                    default:
                        ConsolePrints.PrintErrorInvalid();
                }
            } catch (InputMismatchException e ) {
                ConsolePrints.PrintErrorInvalid();
            }

        }
    }

    //Used to login and check if username and password exist in the backend, if exist move to main menu
    public void logIn()
    {
        User user = checkUserExist();

        if (user != null)
        {
            mainMenu(user);
        }
    }

    //This is used when you want to see if a user exist by inputting a password and username.
    //TODO: DESIGN FLAW, honestly I should break this into 2 functions, one for inputting password and username and another for just checking given a username/password.
    public User checkUserExist()
    {
        String username="", password="";
        User user = null;

        //Enter username
        while ((username.isEmpty() || password.isEmpty()))
        {
            // --------------------  Enter Username -----------------------
            Scanner scanner = new Scanner(System.in);
            if (username.isEmpty())
            {
                System.out.println("\nEnter your username:");
                System.out.println("Type exit to return back to main menu");

                username = scanner.nextLine().trim();

                if (!username.equals("exit"))
                {
                    if (username.isEmpty())
                    {
                        System.err.println("Username cannot be empty!");
                        username="";
                        continue;
                    }
                }
                else {   //if user wants to exit the program
                    break;
                }
            }

            // --------------------  Enter Password -----------------------
            System.out.println("\nEnter your password:");
            System.out.println("Type exit to return back to main menu");
            password = scanner.nextLine();

            if (!password.equals("exit"))
            {
                if (password.isEmpty())
                {
                    System.err.println("password cannot be empty!");
                    password="";
                    continue;
                }
            }
            else {   //if user wants to exit the program
                break;
            }

            // -------------------- Check Database, and login -----------------------
            //Check with database if your logged in or not
            user = userService.Login(username, password);
            if (user == null)
            {
                System.err.println("Username or password is wrong or doesn't exist in the database");
                username="";
                password="";
                continue;
            }
        }

        return user;
    }

    //This is when you are creating a login user
    public void newUser()
    {
        System.out.println("\n\nStarting account creation.");
        boolean exit = false, finished = false;
        String username = "", password = "", passwordConfirm = "-1", firstName = "", lastName = "";

        while (!exit && !finished)
        {
            //  ------------- Enter Username -----------------
            Scanner scanner = new Scanner(System.in);
            if (username.isEmpty()) {
                System.out.println("\nEnter your desired username:");
                System.out.println("Type exit to return back to main menu");

                username = scanner.nextLine().trim();

                if (!username.equals("exit"))
                {
                    if (username.isEmpty())
                    {
                        System.err.println("Username cannot be empty!");
                        username="";
                        continue;
                    }
                    else if(userService.checkUsernameExist(username))
                    {
                        System.err.println("Username taken.");
                        username="";
                        continue;
                    }
                }
                else {   //if user wants to exit the program
                    exit = true;
                    return;
                }
            }

            while (!password.equals(passwordConfirm) && !exit)
            {
                //  ------------- Enter Password -----------------
                System.out.println("\nEnter your desired password:");
                System.out.println("Type exit to return back to main menu");
                password = scanner.nextLine();

                if (!password.equals("exit"))
                {  //Password checks goes here
                    if (password.isEmpty())
                    {
                        System.err.println("Password cannot be empty!");
                        password="";
                        continue;
                    }
                }
                else {   //if user wants to exit the program
                    exit = true;
                    return;
                }

                //  ------------- Reenter password  -----------------
                System.out.println("\nReenter your password:");
                System.out.println("Type exit to return back to main menu");
                passwordConfirm = scanner.nextLine();

                if (!passwordConfirm.equals("exit"))
                {  //Password checks goes here
                    if (!passwordConfirm.equals(password))
                    {
                        System.err.println("Password doesn't match!");
                        password="";
                        passwordConfirm="-1";
                        continue;
                    }
                }
                else {   //if user wants to exit the program
                    exit = true;
                    return;
                }
            }

            //  ------------- Enter First Name -----------------
            System.out.println("\nEnter your first name:");
            System.out.println("Type exit to return back to main menu");

            firstName = scanner.nextLine().trim();

            if (!firstName.equals("exit"))
            {
                if (firstName.isEmpty())
                {
                    System.err.println("First name cannot be empty!");
                    firstName="";
                    continue;
                }
            }
            else {   //if user wants to exit the program
                exit = true;
                return;
            }

            //  ------------- Enter Last Name -----------------
            System.out.println("\nEnter your last name:");
            System.out.println("Type exit to return back to main menu");

            lastName = scanner.nextLine().trim();

            if (!lastName.equals("exit"))
            {
                if (lastName.isEmpty())
                {
                    System.err.println("Last name cannot be empty!");
                    lastName="";
                    continue;
                }
            }
            else {   //if user wants to exit the program
                exit = true;
                return;
            }

            //  ------------- Adding the User to the Database -----------------
            userService.addNewUser(username, password, firstName, lastName);
            finished=true;
        }
    }

    //This is the screen when you logged in, where you can check your account, create new banking account, or log out.
    public void mainMenu(User u)
    {

        boolean exit = false;

        while (!exit)
        {
            System.out.println("\nWelcome " + u.getLastName() + ", " + u.getFirstName());
            System.out.println("What would you like to do?");
            System.out.println("1. Check Account");
            System.out.println("2. Create new account");
            System.out.println("3. Log out");

            Scanner scanner = new Scanner(System.in);

            try {
                int selection = scanner.nextInt();
                switch (selection) {
                    case 1:
                        CheckAccountAction(u);
                        break; // important for preventing case 2 from also being executed
                    case 2:
                        CreateNewAccount(u);
                        break;
                    case 3:
                        exit = true;
                        break;
                    default:
                        ConsolePrints.PrintErrorInvalid();
                }
            } catch (InputMismatchException e ) { //To catch anything that's not a number
                ConsolePrints.PrintErrorInvalid();
            }
        }
    }

    //This is when you are creating a new bank account for the user.
    public void CreateNewAccount(User u) {
        System.out.println("\nCreating new bank account.");

        //Creating the variables for controlling the console and leaving Account creation
        boolean exit = false, finished = false;
        //Creating Variables needed for making an account
        AccountType accountType = null;

        while (!exit && !finished) {
            System.out.println("\nWhat type of account are you creating?");
            System.out.println("1. SAVINGS");
            System.out.println("2. CHECKING");
            System.out.println("3. JOINT");
            System.out.println("4. Cancel");

            Scanner scanner = new Scanner(System.in);


            try {
                int selection = scanner.nextInt();
                switch (selection) {
                    case 1:
                        accountType = AccountType.SAVINGS;
                        break; // important for preventing case 2 from also being executed
                    case 2:
                        accountType = AccountType.CHECKING;
                        break;
                    case 3:
                        accountType = AccountType.JOINT;
                        break;
                    case 4:
                        exit = true;
                        break;
                    default:
                        ConsolePrints.PrintErrorInvalid();
                        continue;
                }
                //Now Create the account
                int newAccID = accountService.createAccount(u, accountType);
                System.out.println("\nAccount Created Successfully!\n");

                //If Joint add more user accounts.
                if (accountType == AccountType.JOINT)
                {
                    AddJointAccountUser(newAccID);
                }
                finished = true;

            } catch (InputMismatchException e ) { //To catch anything that's not a number
                ConsolePrints.PrintErrorInvalid();
            }
        }
    }

    //This is to add another user to the Joint bank account
    private void AddJointAccountUser(int accID) {
        boolean exit = false;
        while (!exit) {
            Scanner scanner = new Scanner(System.in);

            System.out.println("You can add more users to this  joint account by logging in.");
            System.out.println("Do you want to add more users to this account?");
            System.out.println("1. Yes");
            System.out.println("2. No");

            try {
                int selection = scanner.nextInt();

                if (selection == 1) {
                    User user = checkUserExist();
                    if (user != null)
                    {
                        accountService.createAccountLink(user, accID);
                        System.out.println("\nUser Linked Successfully!\n");
                    }
                } else if (selection == 2) {
                    exit = true;
                } else {
                    ConsolePrints.PrintErrorInvalid();
                }
            } catch (InputMismatchException e ) { //To catch anything that's not a number
                ConsolePrints.PrintErrorInvalid();
            }
        }
    }

    //This is where user can select an account from a list of all their accounts to perform an action on
    public void CheckAccountAction(User user) {
        //Grabbing all the accounts under this user
        ArrayList<Account> allAccounts = accountService.getAllAccounts(user);

        if (allAccounts.size() == 0)
        {
            System.err.println("\nYou don't have any accounts. Please create an account first.");
        } else {
            //Console informations
            System.out.println("\nSelect an account to check:");

            int counter = 1;
            for (Account a : allAccounts) {
                System.out.println(counter++ + ". Account ID: " + a.getAccountID() + " Account Type: " + a.getAccountType() + " | Balance: $" + a.getBalance());
            }
            System.out.println("Enter anything else to go back.");
            Scanner scanner = new Scanner(System.in);
            try {
                int selection = scanner.nextInt();

                if (selection < 1 || selection > counter - 1) //If it's out of bound
                    ConsolePrints.PrintErrorInvalid();
                else //You choose a correct account to check.
                    ShowAccount(allAccounts.get(selection - 1), user);

            } catch (InputMismatchException e) { //To catch anything that's not a number

            }
        }
    }

    //This is for displaying all the information that account has.
    public void ShowAccount(Account acc, User user)
    {
        //Displaying Account Information
        ConsolePrints.PrintMessage("Your "+ acc.getAccountType() +" account #" + acc.getAccountID() + " has a remaining balance of $"+acc.getBalance());
        if (acc.getAccountType() == AccountType.JOINT)
        {   //If its a joint account, show all the owners
            ConsolePrints.PrintMessage("There are " + acc.getUsers().size() + " owners to this account.");
            for (User u: acc.getUsers())
            {
                ConsolePrints.PrintMessage(" - "+ u.getFirstName() + " " + u.getLastName());
            }
        }

        //Displaying all transactions here
        ArrayList<Transaction> allTransaction = accountService.getAllTransactions(acc);
        if (allTransaction.size() == 0)
        {
            ConsolePrints.PrintMessage("You have no transactions on this account.");
        }
        else
        {
            ConsolePrints.PrintMessage("\nHere is all your transactions.");
            for (Transaction t: allTransaction)
            {
                ConsolePrints.PrintMessage(" - " + t);
            }
        }

        //Displaying Action here
        boolean exit = false;

        while (!exit)
        {
            ConsolePrints.PrintMessage("What would you like to do?");
            System.out.println("1. Deposit");
            System.out.println("2. Withdraw");
            System.out.println("3. Transfer");
            System.out.println("4. Exit");

            Scanner scanner = new Scanner(System.in);

            try {
                int selection = scanner.nextInt();
                switch (selection) {
                    case 1:
                        AccountDeposit(acc, user);
                        break; // important for preventing case 2 from also being executed
                    case 2:
                        AccountWithdraw(acc, user);
                        break;
                    case 3:
                        AccountTransfer(acc, user);
                        break;
                    case 4:
                        exit = true;
                        break;
                    default:
                        ConsolePrints.PrintErrorInvalid();
                }
            } catch (InputMismatchException e ) { //To catch anything that's not a number
                ConsolePrints.PrintErrorInvalid();
            }
        }
    }

    //Deposit money into the account, make sure its not negative
    public void AccountDeposit(Account acc, User user)
    {
        BigDecimal amt = BigDecimal.valueOf(0);

        //Find the amt and check if its negative or not, also trim the end if its more than 2 digits
        boolean  finished = false;
        while (!finished)
        {
            ConsolePrints.PrintMessage("\nYou have an account balance of $" + acc.getBalance());
            ConsolePrints.PrintMessage("Please enter the amount you'll like to deposit:");
            ConsolePrints.PrintMessage("(Note: Any decimal place after the first two will be rounded up.)");
            ConsolePrints.PrintMessage("Input 0 to return to previous menu.");

            Scanner scanner = new Scanner(System.in);

            try {
                amt = scanner.nextBigDecimal();

                //Rounding up, so there's only 2 decimal place
                amt = amt.setScale(2, BigDecimal.ROUND_HALF_UP);

                //If it's a negative number ask them to put a positive number.
                if (amt.compareTo(new BigDecimal("0")) < 0) {
                    System.err.println("Please enter a positive number.");
                    continue;
                } else if (amt.compareTo(new BigDecimal("0")) == 0) {
                    break;  //If input 0, leave this menu
                } else {
                    finished = true;
                }

            } catch (InputMismatchException e ) { //To catch anything that's not a number
                ConsolePrints.PrintErrorInvalid();
            }
        }

        accountService.deposit(acc, user, amt);
    }

    //Withdraw money from the account, also check if user even have that much money.
    public void AccountWithdraw(Account acc, User user)
    {
        BigDecimal amt = BigDecimal.valueOf(0);

        //Find the amt and check if its negative or not, also trim the end if its more than 2 digits
        boolean  finished = false;
        while (!finished)
        {
            ConsolePrints.PrintMessage("\nYou have an account balance of $" + acc.getBalance());
            ConsolePrints.PrintMessage("Please enter the amount you'll like to withdraw:");
            ConsolePrints.PrintMessage("(Note: Any decimal place after the first two will be rounded up.)");
            ConsolePrints.PrintMessage("Input 0 to return to previous menu.");

            Scanner scanner = new Scanner(System.in);
            try {
                amt = scanner.nextBigDecimal();

                //Rounding up, so there's only 2 decimal place
                amt = amt.setScale(2, BigDecimal.ROUND_HALF_UP);

                BigDecimal result = acc.getBalance().subtract(amt);

                //If it's a negative number ask them to put a positive number.
                if (amt.compareTo(new BigDecimal("0")) < 0) {
                    System.err.println("Please enter a positive number.");
                    continue;
                } else if (amt.compareTo(new BigDecimal("0")) == 0) {
                    break;  //If input 0, leave this menu
                } else if (result.compareTo(new BigDecimal("0")) < 0) {
                    System.err.println("You cannot withdraw more than your current balance!");
                } else {
                    finished = true;
                }
            } catch (InputMismatchException e ) { //To catch anything that's not a number
                ConsolePrints.PrintErrorInvalid();
            }
        }

        accountService.withdraw(acc, user, amt);
    }

    //When the user wants to transfer money from one account to another
    public void AccountTransfer(Account acc, User user)
    {
        BigDecimal amt = BigDecimal.valueOf(0);
        Account transferAcc = null;

        //Get every account the current user owns
        ArrayList<Account> allAccounts = accountService.getAllAccounts(user);
        ArrayList<Account> possibleAccount = new ArrayList<Account>();

        if (allAccounts.size() < 2)
        {
            System.err.println("Error: No other account to transfer to!");
            return;
        }

        //Now get rid of the selected account, leaving only other accounts
        for (Account a: allAccounts)
        {
            if (a.getAccountID() != acc.getAccountID())
            {
                possibleAccount.add(a);
            }
        }
        //Console informations
        System.out.println("\nSelect an account to check:");

        int counter = 1;
        for (Account a : possibleAccount) {
            System.out.println(counter++ + ". Account ID: " + a.getAccountID() + " Account Type: " + a.getAccountType() + " | Balance: $" + a.getBalance());
        }

        System.out.println("Enter anything else to go back.");

        Scanner scanner = new Scanner(System.in);
        try {
            int selection = scanner.nextInt();

            if (selection < 1 || selection > counter - 1) //If it's out of bound
                ConsolePrints.PrintErrorInvalid();
            else //You choose a correct account to check.
            {  //Now find how much you want to transfer
                boolean  finished = false;
                while (!finished)
                {
                    ConsolePrints.PrintMessage("\nYou have an account balance of $" + acc.getBalance());
                    ConsolePrints.PrintMessage("Please enter the amount you'll like to transfer:");
                    ConsolePrints.PrintMessage("(Note: Any decimal place after the first two will be rounded up.)");
                    ConsolePrints.PrintMessage("Input 0 to return to previous menu.");

                    try {
                        Scanner scanner2 = new Scanner(System.in);
                        amt = scanner2.nextBigDecimal();

                        //Rounding up, so there's only 2 decimal place
                        amt = amt.setScale(2, BigDecimal.ROUND_HALF_UP);

                        BigDecimal result = acc.getBalance().subtract(amt);

                        //If it's a negative number ask them to put a positive number.
                        if (amt.compareTo(new BigDecimal("0")) < 0) {
                            System.err.println("Please enter a positive number.");
                            continue;
                        } else if (amt.compareTo(new BigDecimal("0")) == 0) {
                            break;  //If input 0, leave this menu
                        } else if (result.compareTo(new BigDecimal("0")) < 0) {
                            System.err.println("You cannot withdraw more than your current balance!");
                        } else {
                            finished = true;
                        }
                    } catch (InputMismatchException e ) { //To catch anything that's not a number
                        ConsolePrints.PrintErrorInvalid();
                    }

                }

                if (finished)
                {
                    accountService.transfer(acc, user, amt, possibleAccount.get(selection - 1));
                }
            }

        } catch (InputMismatchException e) {
            //To catch anything that's not a number
        }
    }
}
