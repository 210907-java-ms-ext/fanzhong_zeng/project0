package Bank.services;

import Bank.data.AccountData;
import Bank.models.*;

import java.math.BigDecimal;
import java.util.ArrayList;

public class AccountService {

    private AccountData accData = new AccountData();

    //Given the user, get the Accounts the user have from the server
    public ArrayList<Account> getAllAccounts(User user) {
        return accData.getAllAccounts(user);
    }

    //Create new account and return the accountID
    public int createAccount(User user, AccountType accType) {
        //Creating a new Account
        int accID = accData.CreateAccount(accType);
        //Linking the Account to the User
        createAccountLink(user, accID);

        return accID;
    }

    //Given a user and the account's ID, create a link between the two.
    public void createAccountLink(User user, int accID) {
        accData.CreateAccountLink(user.getUserID(), accID);
    }

    //Given an account, return all the transaction from the database
    public ArrayList<Transaction> getAllTransactions(Account acc) {
        return accData.GetAllTransaction(acc);
    }

    //Given an account, user and an amount, deposit the money into the account and also make a transaction of this action.
    public void deposit(Account acc,User user, BigDecimal amt) {
        //Push the Transaction into the database first;
        accData.CreateNewTransaction(acc, user, amt, TransactionType.DEPOSIT);

        //Now update the Balance of the Account
        BigDecimal total = acc.deposit(amt);
        accData.UpdateAccount(acc, total);
    }

    //Given an account, user and an amount, withdraw the money from the account and also make an transaction of this action.
    public void withdraw(Account acc,User user, BigDecimal amt) {
        //Push the Transaction into the database first;
        accData.CreateNewTransaction(acc, user, amt, TransactionType.WITHDRAW);

        //Now update the Balance of the Account
        BigDecimal total = acc.withdraw(amt);
        accData.UpdateAccount(acc, total);
    }

    //Take money away from the account, and also add money into the transferred account. Also make note of all the transactions.
    public void transfer(Account acc, User user, BigDecimal amt, Account transferAcc) {
        //First create the transaction into the database
        accData.CreateNewTransaction(acc, user, amt, TransactionType.TRANSFER);
        accData.CreateNewTransaction(transferAcc, user, amt, TransactionType.DEPOSIT);

        //Now update the balance of original account
        BigDecimal total = acc.withdraw(amt);
        accData.UpdateAccount(acc, total);

        //Now update the transfered account
        total = transferAcc.deposit(amt);
        accData.UpdateAccount(transferAcc, total);
    }
}
