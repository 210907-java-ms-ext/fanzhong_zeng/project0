package Bank.services;

import Bank.data.UserData;
import Bank.models.User;

public class UserService {

    private UserData userData = new UserData();

    //Given the username and password, retrieve the User from database
    public User Login(String username, String password){
        return userData.Login(username, password);
    }

    //Given the username, check if it already exists in the database.
    public boolean checkUsernameExist(String username){
        return userData.checkUsernameExist(username);
    }

    //Given the user information needed to create a new user in the backend.
    public void addNewUser(String username, String password, String firstName, String lastName) {
        userData.AddNewUser(username, password, firstName, lastName);
    }
}
