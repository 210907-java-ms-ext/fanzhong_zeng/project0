package Bank.models;

public class User {
    private int userID;
    private String username, password, firstName, lastName;


    public User (String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User (int id, String firstName, String lastName)
    {
        userID = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
    public User (int id, String username,String password,  String firstName, String lastName)
    {
        userID = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getUserID() {
        return userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String toString() {
        return "ID: "+ userID + " Username: " + username + " password: " + password + " Name: " + firstName + " " + lastName;
    }
}
