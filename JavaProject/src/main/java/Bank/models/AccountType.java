package Bank.models;

public enum AccountType {
    SAVINGS, CHECKING, JOINT
}
