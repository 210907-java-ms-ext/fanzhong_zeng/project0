package Bank.models;
import java.util.*;
import java.math.BigDecimal;


public class Account {
    private int accountID;
    ArrayList<User> users;
    private AccountType accountType;
    private BigDecimal balance;

    public Account(int accountID,ArrayList<User> users, AccountType accountType, BigDecimal balance)
    {
        this.accountID=accountID;
        this.accountType=accountType;
        this.balance=balance;
        this.users = users;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountID=" + accountID +
                ", users=" + users +
                ", accountType=" + accountType +
                ", balance=" + balance +
                '}';
    }

    //Getters
    public AccountType getAccountType() {
        return accountType;
    }
    public BigDecimal getBalance() {
        return balance;
    }
    public int getAccountID() {
        return accountID;
    }
    public ArrayList<User> getUsers() {
        return users;
    }

    // ---------------- METHODS ---------------------
    //Input the amount you want to withdraw and return the total remaining of the balance
    public BigDecimal withdraw(BigDecimal amt)
    {
        return balance = balance.subtract(amt);
    }

    //Input the amount you want to deposit and return the total amount of the balance.
    public BigDecimal deposit(BigDecimal amt)
    {
        return balance = balance.add(amt);
    }
}
