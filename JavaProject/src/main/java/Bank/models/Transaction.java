package Bank.models;
import java.math.BigDecimal;

public class Transaction {
    private int transactionID, accountID, accountTransferID;
    private BigDecimal amount;
    private TransactionType action;
    User user;

    public Transaction(int transactionID, User user, int accountId, BigDecimal amount, TransactionType action, int accountTransferID)
    {
        this.transactionID = transactionID;
        this.accountID = accountId;
        this.amount = amount;
        this.action = action;
        this.accountTransferID = accountTransferID;

        this.user = user;
    }

    public String toString()
    {
        String result = "TransactionID: " + transactionID +
                " \n      User: "+ user.getFirstName() + " " + user.getLastName() +
                " " + action + " $" + amount +
                (action == TransactionType.DEPOSIT ? " to " : " from ") +
                "account ID: " + accountID;
        if (accountTransferID != 0)
        {
            result += " to account ID: " + accountTransferID;
        }
        return result;
    }
}
