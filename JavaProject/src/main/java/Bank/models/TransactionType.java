package Bank.models;

public enum TransactionType {
    DEPOSIT, WITHDRAW, TRANSFER
}
