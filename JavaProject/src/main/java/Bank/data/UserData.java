package Bank.data;

import Bank.models.User;
import Bank.services.ConnectionService;

import java.sql.*;

public class UserData {
    private ConnectionService connectionService = new ConnectionService();

    //Input the username and password, check if account exist in the backend, if it does, return all the user information
    public User Login(String username, String password) {
        User theUser = null;
        String sql = "select * from users where username = ? AND password = ?";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setString(1, username);
            pstmt.setString(2, password);

            ResultSet result = pstmt.executeQuery();


            if (result.next())
            { //if the user exist
                int id = result.getInt("user_id");
                String firstName = result.getString("first_name");
                String lastName = result.getString("last_name");

                theUser = new User(id, username, password, firstName, lastName);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return theUser;
    }

    //Given an username, check if it already exist in the database.
    public boolean checkUsernameExist(String username) {
        String sql = "select * from users where username = ?";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setString(1, username);

            ResultSet result = pstmt.executeQuery();

            return result.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    //Given the Information needed to create a new user, create one in the backend.
    public void AddNewUser(String username, String password, String firstName, String lastName){
        String sql = "insert into users (username, password, first_name, last_name) values (?, ?, ?, ?)";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            pstmt.setString(3, firstName);
            pstmt.setString(4, lastName);
            pstmt.executeUpdate(); // returns a number indicating the rows that were affected
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
