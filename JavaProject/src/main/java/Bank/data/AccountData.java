package Bank.data;

import Bank.models.*;
import Bank.services.ConnectionService;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

public class AccountData {
    private ConnectionService connectionService = new ConnectionService();

    //Given an user information, return all accounts under his name
    public ArrayList<Account> getAllAccounts(User user) {
        ArrayList<Account> allAccounts = new ArrayList<>();

        String sql = "select * from accounts \n" +
                "\tinner join accountowner on accounts.account_id = accountowner.account_id \n" +
                "\tinner join users on accountowner.user_id = users.user_id \n" +
                "\twhere users.user_id = ? \n" +
                "\torder by accounts.account_id";
        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, user.getUserID());
            ResultSet rs = pstmt.executeQuery();

            //Parsing all accounts into an ArrayList and returning it
            while (rs.next())
            {
                int id = rs.getInt("account_id");
                BigDecimal balance = rs.getBigDecimal("balance");
                AccountType accType = AccountType.valueOf(rs.getString("account_type"));

                //Getting all users from a  type of account (JOINT account if there's any)
                ArrayList<User> allUsers = new ArrayList<User>();
                if (accType == AccountType.JOINT)
                {
                    allUsers = getAllUserFromAccount(id);
                }
                else //If its a normal account, there is only one owner, which is the user
                    allUsers.add(user);

                Account a = new Account(id, allUsers, accType, balance);
                allAccounts.add(a);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return allAccounts;
    }

    //Given an account, return all users thats under this account (Only applies to JOINT accounts)
    private ArrayList<User> getAllUserFromAccount(int accountID){
        ArrayList<User> users = new ArrayList<>();

        String sql = "select u.user_id, u.username, u.password, u.first_name , u.last_name from users u\n" +
                "\tjoin accountowner o on o.user_id = u.user_id \n" +
                "\tjoin accounts a on o.account_id = a.account_id \n" +
                "\twhere a.account_id = ?";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql);
            pstmt.setInt(1, accountID);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next())
            {
                int userID = rs.getInt("user_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");

                User newUser = new User(userID, firstName, lastName);
                users.add(newUser);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return users;
    }

    //Create the account and return the ID of the created Account
    public int CreateAccount(AccountType accType){
        int returnID = -1;

        String sql = "insert into accounts(balance, account_type) values (0, ?::account_types);";
        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, accType.toString());
            int result = pstmt.executeUpdate();

            if (result >= 1)
            {  //Making sure we successfully created something
                ResultSet rs = pstmt.getGeneratedKeys();
                rs.next();

                //Getting the id that was created.
                returnID = rs.getInt(1);
            }
        }   catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return returnID;
    }

    //Create a link between the user and the account under accountowners in the database
    public void CreateAccountLink(int userID, int accID){
        String sql = "insert into accountowner (user_id, account_id) values (?, ?);";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, userID);
            pstmt.setInt(2, accID);
            pstmt.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Given an account, return all the transaction this account has.
    public ArrayList<Transaction> GetAllTransaction(Account acc){
        ArrayList<Transaction> allTransactions = new ArrayList<Transaction>();
        String sql = "select u.first_name, u.last_name, t.transaction_id, t.actions , t.amount, t.account_transfer_id from transactions t \n" +
                "\tinner join accounts a on t.account_id = a.account_id \n" +
                "\tinner join users u on t.user_id = u.user_id \n" +
                "\twhere a.account_id = ? \n" +
                "\torder by t.transaction_id";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, acc.getAccountID());

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                User user = new User(firstName, lastName);

                int transID = rs.getInt("transaction_id");
                TransactionType transType = TransactionType.valueOf(rs.getString("actions"));
                BigDecimal amt= rs.getBigDecimal("amount");
                int accountTransferID = rs.getInt("account_transfer_id");

                Transaction transaction = new Transaction(transID, user, acc.getAccountID(), amt, transType, accountTransferID);
                allTransactions.add(transaction);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return allTransactions;
    }

    //Update Account information with new balance
    public void UpdateAccount(Account acc, BigDecimal newBalance)
    {
        String sql = "update accounts set balance = ? where account_id = ?;";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setBigDecimal(1, newBalance);
            pstmt.setInt(2, acc.getAccountID());
            pstmt.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Create new Transaction for the account
    public void CreateNewTransaction(Account acc, User user, BigDecimal amt, TransactionType action) {
        String sql = "insert into transactions (user_id, account_id, amount, actions) values (?, ?, ?, ?::transaction_type);";

        try {
            Connection c = connectionService.establishConnection();
            PreparedStatement pstmt = c.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, user.getUserID());
            pstmt.setInt(2, acc.getAccountID());
            pstmt.setBigDecimal(3, amt);
            pstmt.setString(4, action.toString());
            pstmt.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


}
