# Project0
For Project 0, I build a console-based banking application. Presentation date: Wednesday, September 29, 2021 (takes the place of QC)

For details regarding the project check [here](Documentation/project-0-requirements.md).
For the backend database please refer to [here](Documentation/BackendSchema.sql).

# Installation
This project is created through a maven IntelliJ Java project. It uses tomcat and PostgreSQL to communicate and create the backend.

# Project Models
These are the basic data types for the project which you can find [here](Documentation/Models.md)

# Backend Connection
You can find the SQL statements and how the application is connected to the backend [here](Documentation/BackendCommunication.md)

# Main Application
You can find the main code of the Application [here](Documentation/MainApp.md)