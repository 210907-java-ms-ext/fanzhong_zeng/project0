# Models
Here is a list of all the data types I created and use.

# Enums

## Account Type
```java
public enum AccountType {
    SAVINGS, CHECKING, JOINT
}
```

## Transaction Type
```java
public enum TransactionType {
    DEPOSIT, WITHDRAW, TRANSFER
}
```

# Java Classes

## User
```java
private int userID;
private String username, password, firstName, lastName;

//Constructors
public User (String firstName, String lastName)
public User (int id, String firstName, String lastName)
public User (int id, String username,String password,  String firstName, String lastName)

//Getters
public int getUserID()
public String getFirstName()
public String getLastName()

//To String
public String toString()
```

## Account
```java
import java.util.*; //For ArrayList
import java.math.BigDecimal;

private int accountID;
ArrayList<User> users;
private AccountType accountType;
private BigDecimal balance;

//Constructors
public Account(int accountID,ArrayList<User> users, AccountType accountType, BigDecimal balance)

//Getters
public int getAccountID()
public ArrayList<User> getUsers()
public BigDecimal getBalance()
public AccountType getAccountType()

//To String
public String toString()

//Methods
public BigDecimal withdraw(BigDecimal amt) //Input the amount you want to withdraw and return the total remaining of the balance
public BigDecimal deposit(BigDecimal amt) //Input the amount you want to deposit and return the total amount of the balance.
```

## Transaction
```java
import java.math.BigDecimal;

//accountID is used to link transaction with accounts in the database
//accountTransferID is used to check which account am I transfering money into
private int transactionID, accountID, accountTransferID;
private BigDecimal amount;
private TransactionType action;
User user;

//Constructor
public Transaction(int transactionID, User user, int accountId, BigDecimal amount, TransactionType action, int accountTransferID)

//To String
public String toString()
```