create type account_types as enum('SAVINGS', 'CHECKING', 'JOINT');
create type transaction_type as enum('DEPOSIT', 'WITHDRAW', 'TRANSFER');

create table users (
	user_id serial primary key,
	username varchar(50) not null unique,
	password varchar(50) not null,
	first_name varchar(50),
	last_name varchar(50) not null
);

create table accounts (
	account_id serial primary key,
	balance decimal(12, 2),
	account_type account_types
);

create table accountOwner (
	owner_id serial primary key,
	user_id int references users(user_id) not null,
	account_id int references accounts(account_id) not null
);

create table transactions ( 
	transaction_id serial primary key,
	user_id int references users(user_id) not null,
	account_id int references accounts(account_id) not null,
	account_transfer_id int references accounts(account_id),
	amount money not null,
	actions transaction_type not null
);
