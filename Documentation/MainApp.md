# Models
Here is the main code for the application.

# Java Classes

## ConsolePrints
Intend to be used as a better method of printing message to the console, however was never fully implemented.
```java
public static void PrintMessage(String msg)
public static void PrintErrorInvalid()
```

## Bank Main
```java
import Bank.models.Account;
import Bank.models.AccountType;
import Bank.models.Transaction;
import Bank.models.User;
import Bank.services.AccountService;
import Bank.services.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

//The Controllers for the backend
UserService userService = new UserService();
AccountService accountService = new AccountService();

//First screen you see when the app is launched. Givens 3 options of login, create new account, or exit program
public void menu()

//Used to login and check if username and password exist in the backend, if exist move to main menu
public void logIn()

//This is used when you want to see if a user exist by inputting a password and username.
//TODO: DESIGN FLAW, honestly I should break this into 2 functions, one for inputting password and username and another for just checking given a username/password.
public User checkUserExist()


//This is when you are creating a login user
public void newUser()

//This is the screen when you logged in, where you can check your account, create new banking account, or log out.
public void mainMenu(User u)

//This is when you are creating a new bank account for the user.
public void CreateNewAccount(User u)

//This is to add another user to the Joint bank account
private void AddJointAccountUser(int accID)

//This is where user can select an account from a list of all their accounts to perform an action on
public void CheckAccountAction(User user)

//This is for displaying all the information that account has.
public void ShowAccount(Account acc, User user)

//Deposit money into the account, make sure its not negative
public void AccountDeposit(Account acc, User user)

//Withdraw money from the account, also check if user even have that much money.
public void AccountWithdraw(Account acc, User user)

//When the user wants to transfer money from one account to another
public void AccountTransfer(Account acc, User user)
```