# Backend Connection
This is split between getting data from database using SQL commands and a Service controller class to tell the backend communicating class what to do.

# Backend Communicating Classes
This is the classes that's responsible of communicating to the backend using SQL.

## User Data
```java
import Bank.models.User;
import Bank.services.ConnectionService;
import java.sql.*;

private ConnectionService connectionService = new ConnectionService();

//Input the username and password, check if account exist in the backend, if it does, return all the user information
public User Login(String username, String password)

//Given an username, check if it already exist in the database.
public boolean checkUsernameExist(String username)

//Given the Information needed to create a new user, create one in the backend.
public void AddNewUser(String username, String password, String firstName, String lastName)
```

## Account Data
```java
import Bank.models.*;
import Bank.services.ConnectionService;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;

private ConnectionService connectionService = new ConnectionService();

//Given an user information, return all accounts under his name
public ArrayList<Account> getAllAccounts(User user)

//Given an account, return all users thats under this account (Only applies to JOINT accounts)
private ArrayList<User> getAllUserFromAccount(int accountID)

//Create the account and return the ID of the created Account
public int CreateAccount(AccountType accType)

//Create a link between the user and the account under accountowners in the database
public void CreateAccountLink(int userID, int accID)

//Given an account, return all the transaction this account has.
public ArrayList<Transaction> GetAllTransaction(Account acc)

//Update Account information with new balance
public void UpdateAccount(Account acc, BigDecimal newBalance)

//Create new Transaction for the account
public void CreateNewTransaction(Account acc, User user, BigDecimal amt, TransactionType action)
```

# Controller Classes
Used to control how to talk to the backend.

## ConectionService
```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//Used to establish a connection between this app and backend. It's also where the server information (user and pass) is used.
public Connection establishConnection()
```

## User Service
```java
import Bank.data.UserData;
import Bank.models.User;

private UserData userData = new UserData();

//Given the username and password, retrieve the User from database
public User Login(String username, String password)

//Given the username, check if it already exists in the database.
public boolean checkUsernameExist(String username)

//Given the user information needed to create a new user in the backend.
public void addNewUser(String username, String password, String firstName, String lastName)
```

## Account Service
```java
import Bank.data.AccountData;
import Bank.models.*;

import java.math.BigDecimal;
import java.util.ArrayList;

private AccountData accData = new AccountData();

//Given the user, get the Accounts the user have from the server
public ArrayList<Account> getAllAccounts(User user)

//Create new account and return the accountID
public int createAccount(User user, AccountType accType)

//Given a user and the account's ID, create a link between the two.
public void createAccountLink(User user, int accID)

//Given an account, return all the transaction from the database
public ArrayList<Transaction> getAllTransactions(Account acc)

//Given an account, user and an amount, deposit the money into the account and also make a transaction of this action.
public void deposit(Account acc,User user, BigDecimal amt)

//Given an account, user and an amount, withdraw the money from the account and also make an transaction of this action.
public void withdraw(Account acc,User user, BigDecimal amt)

//Take money away from the account, and also add money into the transferred account. Also make note of all the transactions.
public void transfer(Account acc, User user, BigDecimal amt, Account transferAcc) 
```